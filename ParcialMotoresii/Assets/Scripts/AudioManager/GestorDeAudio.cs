using System;
using UnityEngine;

public class GestorDeAudio : MonoBehaviour
{
    public Singleton[] sonidos;
    public static GestorDeAudio instancia;

    void Awake()
    {
        if (instancia == null)
        {
            instancia = this;
        }
        else
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
        foreach (Singleton s in sonidos)
        {
            s.FuenteAudio = gameObject.AddComponent<AudioSource>();
            s.FuenteAudio.clip = s.ClipSonido;
            s.FuenteAudio.volume = s.Volumen;
            s.FuenteAudio.pitch = s.pitch;
            s.FuenteAudio.loop = s.repetir;
        }
    }

    public void ReproducirSonido(string nombre)
    {
        Singleton s = Array.Find(sonidos, sound => sound.Nombre == nombre);
        if (s == null)
        {
            Debug.LogWarning("Sonido: " + nombre + " no encontrado.");
            return;
        }
        else
        {
            s.FuenteAudio.Play();
        }
    }

    public void PausarSonido(string nombre)
    {
        Singleton s = Array.Find(sonidos, sonido => sonido.Nombre == nombre);
        if (s == null)
        {
            Debug.LogWarning("Sonido: " + nombre + " no encontrado.");
            return;
        }
        else
        {
            s.FuenteAudio.Pause();
        }
    }
}