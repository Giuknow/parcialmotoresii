using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jump : MonoBehaviour
{
    public float alturaDeSalto = 5;
    public Rigidbody body;
    void Start()
    {
        body = GetComponent<Rigidbody>();
    }


    void Update()
    {

        if (MenuScreen.play == 1)
        {
            Saltar();
        }
    }

    public void Saltar()
    {
        if (Input.GetKeyDown(KeyCode.Space)) 
        {
            body.AddForce(Vector3.up * alturaDeSalto, ForceMode.Impulse);
        }
    }
}
