using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoJugador : MonoBehaviour
{
    public float v_mov = 10.0f;

    void Start()
    {

    }

    void Update()
    {

        if (MenuScreen.play == 1)
        {
            ControlMovimiento();
        }
    }

    public void ControlMovimiento()
    {
            float movimientoAdelanteAtras = Input.GetAxis("Vertical") * v_mov;
            float movimientoCostados = Input.GetAxis("Horizontal") * v_mov;
            movimientoAdelanteAtras *= Time.deltaTime;
            movimientoCostados *= Time.deltaTime;
            transform.Translate(movimientoCostados, 0, movimientoAdelanteAtras);
    }
}
