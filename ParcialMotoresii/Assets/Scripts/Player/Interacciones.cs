using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interacciones : MonoBehaviour
{
    public bool object_CanBePicked = false;
    public GameObject itemToPick;
    public GameObject itemPicked;
    public static int pickCounter = 0;
    void Start()
    {
        itemPicked.SetActive(false);
    }
    void Update()
    {
        PickItems();
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            object_CanBePicked = true;
        }
    }
    public void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player") == false)
        {
            object_CanBePicked = false;
        }
    }

    private void PickItems()
    {
        if (object_CanBePicked && Input.GetKeyDown(KeyCode.E)) 
        {
            itemToPick.SetActive(false);
            itemPicked.SetActive(true);
            GestorDeAudio.instancia.ReproducirSonido("Grab");
            pickCounter++;
            
        }
    }

}
