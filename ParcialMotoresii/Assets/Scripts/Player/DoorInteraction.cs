using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorInteraction : MonoBehaviour
{
    public int compare;
    public bool doorCanBeOpened;
    public static int doorCount = 0;
    public GameObject door;
    public GameObject door_Opened;
    public GameObject keyInHand;
    void Start()
    {
        door_Opened.SetActive(false);
    }
    void Update()
    {
        DoorCollider();
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            doorCanBeOpened = true;
        }
    }

    public void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            doorCanBeOpened = false;
        }
    }

    public void DoorCollider()
    {
        if (doorCanBeOpened && Input.GetKeyDown(KeyCode.E) && Interacciones.pickCounter == compare) 
        {
            keyInHand.SetActive(false);
            door_Opened.SetActive(true);
            door.SetActive(false);
            doorCount++;
            GestorDeAudio.instancia.ReproducirSonido("Door");
        }
    }
}
