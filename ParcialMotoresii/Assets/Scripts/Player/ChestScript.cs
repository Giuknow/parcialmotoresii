using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChestScript : MonoBehaviour
{
    public bool ChestCanBeOpened = false;
    public GameObject ChestTop;
    public GameObject OpenedChest;
    public GameObject ghostInChest;
    public GameObject KeyPicked;
    void Start()
    {
        OpenedChest.SetActive(false);
        ghostInChest.SetActive(false);
    }
    void Update()
    {
        PickItems();
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            ChestCanBeOpened = true;
        }
    }
    public void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player") == false)
        {
            ChestCanBeOpened = false;
        }
    }

    private void PickItems()
    {
        if (ChestCanBeOpened && Input.GetKeyDown(KeyCode.E) && Interacciones.pickCounter == 7 && Narrador.counter == 8)
        {
            ChestTop.SetActive(false);
            OpenedChest.SetActive(true);
            ghostInChest.SetActive(true);
            KeyPicked.SetActive(false);
            Narrador.counter++;
            Narrador.control = true;
            GestorDeAudio.instancia.ReproducirSonido("Door");
        }
    }

}
