using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwapMirror : MonoBehaviour
{
    public bool invert = false;
    public GameObject Reflection;
    public GameObject InvertedReflection;
    public GameObject Bathroom;
    public GameObject InvertedBathroom;

    void Start()
    {
        InvertedBathroom.SetActive(false);
        InvertedReflection.SetActive(false);
    }

    void Update()
    {
        InvertInput();
    }

    public void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            invert = true;
        }
        else
        {
            invert = false;
        }
    }

    private void InvertInput()
    {
        if (invert && Input.GetKeyUp(KeyCode.E)) 
        {
            GestorDeAudio.instancia.ReproducirSonido("Grab");
            Narrador.MirrorIsInverted = true;
            ToiletAnimation.anim = true;
            InvertedBathroom.SetActive(true);
            InvertedReflection.SetActive(true);
            Bathroom.SetActive(false);
            Reflection.SetActive(false);
            
        }
    }
}
