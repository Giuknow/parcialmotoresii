using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatorManager : MonoBehaviour
{
    public static bool event1 = false;
    public Animator animator1; //Trapdoor
    public Animator animator2; //Machine
    void Start()
    {

    }


    void Update()
    {
        AnimarMaquina();
    }

    private void AnimarMaquina()
    {
        if (event1)
        {
            StartCoroutine(Event1());
        }
    }

    IEnumerator Event1()
    {
        event1 = false;
        yield return new WaitForSeconds(50f);
        GestorDeAudio.instancia.ReproducirSonido("Machine");
        animator1.Play("Trapdoor");
        animator2.Play("Machine");
        yield return new WaitForSeconds(10f);
        Narrador.control = true;
        Narrador.counter++;
    }
}
