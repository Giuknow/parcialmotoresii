using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    void Start()
    {
        Cursor.lockState = CursorLockMode.None;
    }
    void Update()
    {
        if (MenuScreen.play == 1)
            Cursor.lockState = CursorLockMode.Locked;

        CursorESC(false);
    }

    private void CursorESC(bool vCursorActivado)
    {
        if (Input.GetKeyUp(KeyCode.Escape) && MenuScreen.play == 1)
        {
            vCursorActivado = !vCursorActivado;

            if (vCursorActivado == false)
            {
                Cursor.lockState = CursorLockMode.None;
            }
            else
            {
                Cursor.lockState = CursorLockMode.Locked;
            }
        }
    }  
}
