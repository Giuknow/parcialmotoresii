using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GiantKey : MonoBehaviour
{
   public bool animationStarted = false;
    public bool control = false;
    public Animator animator;
    void Start()
    {
        
    }

    
    void Update()
    {

        if(control)
        PushKey();
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            control = true;
        }
    }
    private void PushKey()
    {
        if (Input.GetKeyDown(KeyCode.E) && animationStarted == false) 
        {
            animationStarted = true;
            animator.Play("GiantKey");
        }
    }
}
