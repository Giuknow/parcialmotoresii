using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Narrador : MonoBehaviour
{
    public bool SKIP = false;
    public static int counter = 0;
    public static bool control;
    public static bool MirrorIsInverted;
    public TextMeshProUGUI narrador;
    public TextMeshProUGUI yellow;
    public TextMeshProUGUI red;
    public TextMeshProUGUI Bk;
    public GameObject GiantKey2;
    public GameObject HouseCollider;
    public GameObject HouseCollider2;
    public GameObject LittleHouse;
    void Start()
    {
        GiantKey2.SetActive(false);
        HouseCollider.SetActive(false);
        HouseCollider2.SetActive(false);
        LittleHouse.SetActive(false);
        control = true;
        narrador.text = "";
        yellow.text = "";
        red.text = "";
        Bk.text = "";
    }
    void Update()
    {
        NarradorSubtitulos();
    }

    private void NarradorSubtitulos()
    {
        StartCoroutine(narradorCoroutine());
        StartCoroutine(narradorCoroutine2());
    }

    IEnumerator narradorCoroutine()
    {
        if (control && counter == 0 && MenuScreen.play == 1 && SKIP == false)
        {
            control = false;
            yield return new WaitForSeconds(5);
            GestorDeAudio.instancia.ReproducirSonido("Narrador");
            narrador.text = "�Ey! �Hola ni�o, bienvenido otra vez!";
            yield return new WaitForSeconds(3);
            GestorDeAudio.instancia.PausarSonido("Narrador");
            narrador.text = "";

            yield return new WaitForSeconds(1);
            GestorDeAudio.instancia.ReproducirSonido("Narrador");
            narrador.text = "Seguro te estar�s preguntando por qu� una voz misteriosa te habla en tu cuarto a mitad de la noche...";
            yield return new WaitForSeconds(7);
            GestorDeAudio.instancia.PausarSonido("Narrador");
            narrador.text = "";

            yield return new WaitForSeconds(1);
            GestorDeAudio.instancia.ReproducirSonido("Narrador");
            narrador.text = "Yo soy tu conciencia de sue�os.";
            yield return new WaitForSeconds(2.5f);
            GestorDeAudio.instancia.PausarSonido("Narrador");
            narrador.text = "";

            yield return new WaitForSeconds(1);
            GestorDeAudio.instancia.ReproducirSonido("Narrador");
            narrador.text = "Ahora mismo nos encontramos en un espacio intermediario entre tus sue�os y el mundo real.";
            yield return new WaitForSeconds(7f);
            GestorDeAudio.instancia.PausarSonido("Narrador");
            narrador.text = "";

            yield return new WaitForSeconds(1);
            GestorDeAudio.instancia.ReproducirSonido("Narrador");
            narrador.text = "Pero, hay un peque�o... problema.";
            yield return new WaitForSeconds(4f);
            GestorDeAudio.instancia.PausarSonido("Narrador");
            narrador.text = "";

            yield return new WaitForSeconds(1);
            GestorDeAudio.instancia.ReproducirSonido("Narrador");
            narrador.text = "Uno de mis ayudantes, el que viste un tapado de color negro, se ha... extraviado.";
            yield return new WaitForSeconds(7f);
            GestorDeAudio.instancia.PausarSonido("Narrador");
            narrador.text = "";

            yield return new WaitForSeconds(1);
            GestorDeAudio.instancia.ReproducirSonido("Narrador");
            narrador.text = "Y este problema no tiene nada que ver contigo, exceptuando el hecho de que �l es quien se encarga de despertarte... �Entiendes?";
            yield return new WaitForSeconds(9f);
            GestorDeAudio.instancia.PausarSonido("Narrador");
            narrador.text = "";

            yield return new WaitForSeconds(1);
            GestorDeAudio.instancia.ReproducirSonido("Narrador");
            narrador.text = "Entonces, no nos queda otra alternativa que encontrarlo lo antes posible.";
            yield return new WaitForSeconds(6f);
            GestorDeAudio.instancia.PausarSonido("Narrador");
            narrador.text = "";

            yield return new WaitForSeconds(1);
            GestorDeAudio.instancia.ReproducirSonido("Narrador");
            narrador.text = "�Comencemos!";
            yield return new WaitForSeconds(3f);
            GestorDeAudio.instancia.PausarSonido("Narrador");
            narrador.text = "";

            yield return new WaitForSeconds(1);
            GestorDeAudio.instancia.ReproducirSonido("Narrador");
            narrador.text = "Busca la manera de salir de esta habitaci�n.";
            yield return new WaitForSeconds(5f);
            GestorDeAudio.instancia.PausarSonido("Narrador");
            narrador.text = "";

        }
    }

    IEnumerator narradorCoroutine2()
    {
        if (control == true && counter == 1)
        {
            control = false;
            yield return new WaitForSeconds(8);
            GestorDeAudio.instancia.ReproducirSonido("Narrador");
            narrador.text = "Esta es la Central Mental.";
            yield return new WaitForSeconds(4f);
            GestorDeAudio.instancia.PausarSonido("Narrador");
            narrador.text = "";

            yield return new WaitForSeconds(1);
            GestorDeAudio.instancia.ReproducirSonido("Narrador");
            narrador.text = "Aqu� guardamos toda la informaci�n sobre todos y cada uno de tus sue�os.";
            yield return new WaitForSeconds(5f);
            GestorDeAudio.instancia.PausarSonido("Narrador");
            narrador.text = "";

            yield return new WaitForSeconds(1);
            GestorDeAudio.instancia.ReproducirSonido("Narrador");
            narrador.text = "Todos estos dibujos los has hecho aqu�, aunque probablemente no lo recuerdes...";
            yield return new WaitForSeconds(7f);
            GestorDeAudio.instancia.PausarSonido("Narrador");
            narrador.text = "";

            yield return new WaitForSeconds(1);
            GestorDeAudio.instancia.ReproducirSonido("Narrador");
            narrador.text = "�Eres todo un artista!";
            yield return new WaitForSeconds(3f);
            GestorDeAudio.instancia.PausarSonido("Narrador");
            narrador.text = "";

            yield return new WaitForSeconds(2);
            GestorDeAudio.instancia.ReproducirSonido("Narrador");
            narrador.text = "Si volteas ver�s que la habitaci�n de la que saliste era en realidad una peque�a casa de juguete...";
            yield return new WaitForSeconds(8f);
            GestorDeAudio.instancia.PausarSonido("Narrador");
            narrador.text = "";

            yield return new WaitForSeconds(1);
            GestorDeAudio.instancia.ReproducirSonido("Narrador");
            narrador.text = "�Esa es la magia de los sue�os!";
            yield return new WaitForSeconds(3f);
            GestorDeAudio.instancia.PausarSonido("Narrador");
            narrador.text = "";
        }

        if (control == true && counter == 2)
        {
            control = false;
            yield return new WaitForSeconds(1);
            GestorDeAudio.instancia.ReproducirSonido("Narrador");
            narrador.text = "�Por supuesto!";
            yield return new WaitForSeconds(3);
            GestorDeAudio.instancia.PausarSonido("Narrador");
            narrador.text = "";

            yield return new WaitForSeconds(1);
            GestorDeAudio.instancia.ReproducirSonido("Narrador");
            narrador.text = "Esa m�quina de ah� es EL OJO.";
            yield return new WaitForSeconds(4);
            GestorDeAudio.instancia.PausarSonido("Narrador");
            narrador.text = "";

            yield return new WaitForSeconds(1);
            GestorDeAudio.instancia.ReproducirSonido("Narrador");
            narrador.text = "Su funci�n es alterar la realidad mediante el uso de la perspectiva.";
            yield return new WaitForSeconds(6);
            GestorDeAudio.instancia.PausarSonido("Narrador");
            narrador.text = "";

            yield return new WaitForSeconds(1);
            GestorDeAudio.instancia.ReproducirSonido("Narrador");
            narrador.text = "�Y mira! �Ah� est� uno de mis ayudantes!";
            yield return new WaitForSeconds(4);
            GestorDeAudio.instancia.PausarSonido("Narrador");
            narrador.text = "";

            yield return new WaitForSeconds(1);
            GestorDeAudio.instancia.ReproducirSonido("Narrador");
            narrador.text = "Pero es muy... peque�o. �Qu� le habr� pasado?";
            yield return new WaitForSeconds(6);
            GestorDeAudio.instancia.PausarSonido("Narrador");
            narrador.text = "";

            yield return new WaitForSeconds(3);
            GestorDeAudio.instancia.ReproducirSonido("Narrador");
            narrador.text = "Coloca al ayudante Amarillo en EL OJO. Puede que usando la perspectiva podamos devolverlo a su tama�o normal.";
            yield return new WaitForSeconds(8);
            GestorDeAudio.instancia.PausarSonido("Narrador");
            narrador.text = "";

        }

        if (control == true && counter == 3)
        {
            control = false;
            yield return new WaitForSeconds(2);
            GestorDeAudio.instancia.ReproducirSonido("Narrador");
            narrador.text = "�Perfecto! Ahora solo queda buscar un buen �ngulo.";
            yield return new WaitForSeconds(7);
            GestorDeAudio.instancia.PausarSonido("Narrador");
            narrador.text = "";

            yield return new WaitForSeconds(2);
            GestorDeAudio.instancia.ReproducirSonido("Narrador");
            narrador.text = "Mira a trav�s del PANEL utilizando la perspectiva para posar al ayudante sobre el CUBO AMARILLO de all� atr�s.";
            yield return new WaitForSeconds(10);
            GestorDeAudio.instancia.PausarSonido("Narrador");
            narrador.text = "";
        }

        if (control == true && counter == 4)
        {
            control = false;
            yield return new WaitForSeconds(1);
            GestorDeAudio.instancia.ReproducirSonido("Narrador");
            narrador.text = "�Fant�stico, lo has entendido! Ahora pregunt�mosle si sabe algo sobre el ayudante extraviado.";
            yield return new WaitForSeconds(8);
            GestorDeAudio.instancia.PausarSonido("Narrador");
            narrador.text = "";

            yield return new WaitForSeconds(3);
            GestorDeAudio.instancia.ReproducirSonido("Narrador");
            narrador.text = "Oye, amarillito... �Sabes d�nde est� tu compa�ero de color negro?";
            yield return new WaitForSeconds(5);
            GestorDeAudio.instancia.PausarSonido("Narrador");
            narrador.text = "";

            yield return new WaitForSeconds(1);
            GestorDeAudio.instancia.ReproducirSonido("Narrador");
            narrador.text = "Lo necesitamos para que el ni�o pueda despertarse.";
            yield return new WaitForSeconds(4);
            GestorDeAudio.instancia.PausarSonido("Narrador");
            narrador.text = "";

            yield return new WaitForSeconds(1);
            GestorDeAudio.instancia.ReproducirSonido("Yellow");
            yellow.text = "No se nada al respecto.";
            yield return new WaitForSeconds(4);
            GestorDeAudio.instancia.PausarSonido("Yellow");
            yellow.text = "";

            yield return new WaitForSeconds(1);
            GestorDeAudio.instancia.ReproducirSonido("Narrador");
            narrador.text = "�Nada? �No sabes nada?";
            yield return new WaitForSeconds(3);
            GestorDeAudio.instancia.PausarSonido("Narrador");
            narrador.text = "";

            yield return new WaitForSeconds(1);
            GestorDeAudio.instancia.ReproducirSonido("Yellow");
            yellow.text = "Podr�a estar en la cocina, pasando la puerta marr�n de la izquierda, pero no estoy seguro...";
            yield return new WaitForSeconds(7);
            GestorDeAudio.instancia.PausarSonido("Yellow");
            yellow.text = "";

            yield return new WaitForSeconds(1);
            GestorDeAudio.instancia.ReproducirSonido("Narrador");
            narrador.text = "Bueno, eso es algo. �D�nde esta la llave de esa puerta?";
            yield return new WaitForSeconds(5);
            GestorDeAudio.instancia.PausarSonido("Narrador");
            narrador.text = "";

            yield return new WaitForSeconds(1);
            GestorDeAudio.instancia.ReproducirSonido("Yellow");
            yellow.text = "Mi compa�ero de color ROJO sabe todo acerca de llaves, pero tampoco lo he visto.";
            yield return new WaitForSeconds(6);
            GestorDeAudio.instancia.PausarSonido("Yellow");
            yellow.text = "";

            yield return new WaitForSeconds(1);
            GestorDeAudio.instancia.ReproducirSonido("Narrador");
            narrador.text = "�Caracoles! Bueno, no tuvo caso ni�o... �Se te ocurre algo?";
            yield return new WaitForSeconds(7);
            GestorDeAudio.instancia.PausarSonido("Narrador");
            narrador.text = "";
        }

        if (control == true && counter == 5)
        {
            control = false;
            yield return new WaitForSeconds(1);
            GestorDeAudio.instancia.ReproducirSonido("Narrador");
            narrador.text = "�Brillante! Quiz�s haya algo interesante en la casa.";
            yield return new WaitForSeconds(4);
            GestorDeAudio.instancia.PausarSonido("Narrador");
            narrador.text = "";

            yield return new WaitForSeconds(1);
            GestorDeAudio.instancia.ReproducirSonido("Narrador");
            narrador.text = "Debe ser lo suficientemente grande como para que puedas entrar.";
            yield return new WaitForSeconds(5);
            GestorDeAudio.instancia.PausarSonido("Narrador");
            narrador.text = "";
        }

        if (control == true && counter == 6)
        {
            control = false;
            yield return new WaitForSeconds(1);
            GestorDeAudio.instancia.ReproducirSonido("Narrador");
            narrador.text = "�Magnifico! Veamos si hay algo dentro.";
            yield return new WaitForSeconds(5);
            GestorDeAudio.instancia.PausarSonido("Narrador");
            narrador.text = "";
        }

        if (control == true && counter == 7)
        {
            control = false;
            yield return new WaitForSeconds(1);
            GestorDeAudio.instancia.ReproducirSonido("Narrador");
            narrador.text = "Parecen ser fragmentos de una llave...";
            yield return new WaitForSeconds(5);
            GestorDeAudio.instancia.PausarSonido("Narrador");
            narrador.text = "";
        }

        if (control == true && counter == 8)
        {
            control = false;
            yield return new WaitForSeconds(1);
            GestorDeAudio.instancia.ReproducirSonido("Narrador");
            narrador.text = "Efectivamente es una llave, pero no del tama�o correcto para entrar en la cerradura...";
            yield return new WaitForSeconds(6);
            GestorDeAudio.instancia.PausarSonido("Narrador");
            narrador.text = "";

            yield return new WaitForSeconds(1);
            GestorDeAudio.instancia.ReproducirSonido("Narrador");
            narrador.text = "Debe servir para otra cosa.";
            yield return new WaitForSeconds(4);
            GestorDeAudio.instancia.PausarSonido("Narrador");
            narrador.text = "";
        }

        if (control == true && counter == 9)
        {
            control = false;
            yield return new WaitForSeconds(1);
            GestorDeAudio.instancia.ReproducirSonido("Narrador");
            narrador.text = "�Mira! �El otro ayudante!";
            yield return new WaitForSeconds(3);
            GestorDeAudio.instancia.PausarSonido("Narrador");
            narrador.text = "";
        }

        if (control == true && counter == 10)
        {
            control = false;
            yield return new WaitForSeconds(3);
            GestorDeAudio.instancia.ReproducirSonido("Narrador");
            narrador.text = "Hola peque�o amigo. �Puedes oirme?";
            yield return new WaitForSeconds(5);
            GestorDeAudio.instancia.PausarSonido("Narrador");
            narrador.text = "";

            yield return new WaitForSeconds(1);
            GestorDeAudio.instancia.ReproducirSonido("Yellow");
            red.text = "�Fuerte y claro!";
            yield return new WaitForSeconds(3);
            GestorDeAudio.instancia.PausarSonido("Yellow");
            red.text = "";

            yield return new WaitForSeconds(1);
            GestorDeAudio.instancia.ReproducirSonido("Narrador");
            narrador.text = "Excelente. Uno de tus compa�eros se perdi� y el amarillo nos dijo que puede estar en la cocina...";
            yield return new WaitForSeconds(6);
            GestorDeAudio.instancia.PausarSonido("Narrador");
            narrador.text = "";

            yield return new WaitForSeconds(1);
            GestorDeAudio.instancia.ReproducirSonido("Narrador");
            narrador.text = "Y nos pregunt�bamos si tienes idea de d�nde est� la llave para abrir la puerta.";
            yield return new WaitForSeconds(5);
            GestorDeAudio.instancia.PausarSonido("Narrador");
            narrador.text = "";

            yield return new WaitForSeconds(1);
            GestorDeAudio.instancia.ReproducirSonido("Yellow");
            red.text = "�Bueno, hoy es su d�a de suerte! Se exactamente donde est� la llave que est�n buscando.";
            yield return new WaitForSeconds(6);
            GestorDeAudio.instancia.PausarSonido("Yellow");
            red.text = "";

            yield return new WaitForSeconds(1);
            GestorDeAudio.instancia.ReproducirSonido("Narrador");
            narrador.text = "Bueno, eso fue r�pido...";
            yield return new WaitForSeconds(3);
            GestorDeAudio.instancia.PausarSonido("Narrador");
            narrador.text = "";

            yield return new WaitForSeconds(0.5f);
            GestorDeAudio.instancia.ReproducirSonido("Yellow");
            red.text = "Est� en el techo.";
            yield return new WaitForSeconds(2);
            GestorDeAudio.instancia.PausarSonido("Yellow");
            red.text = "";

            yield return new WaitForSeconds(0.5f);
            GestorDeAudio.instancia.ReproducirSonido("Narrador");
            narrador.text = "�En el techo? �Es cierto! �C�mo es que no  la hemos visto antes?";
            yield return new WaitForSeconds(6);
            GestorDeAudio.instancia.PausarSonido("Narrador");
            narrador.text = "";

            yield return new WaitForSeconds(0.5f);
            GestorDeAudio.instancia.ReproducirSonido("Yellow");
            red.text = "Eso es porque acaba de aparecer.";
            yield return new WaitForSeconds(3);
            GestorDeAudio.instancia.PausarSonido("Yellow");
            red.text = "";

            yield return new WaitForSeconds(0.5f);
            GestorDeAudio.instancia.ReproducirSonido("Narrador");
            narrador.text = "Bueno... pero, esa llave es gigante. Y adem�s, no est� al alcance del ni�o...";
            yield return new WaitForSeconds(6);
            GestorDeAudio.instancia.PausarSonido("Narrador");
            narrador.text = "";

            yield return new WaitForSeconds(0.5f);
            GestorDeAudio.instancia.ReproducirSonido("Yellow");
            red.text = "Podr�as escalar los bloques de contrucci�n para alcanzarla.";
            yield return new WaitForSeconds(4);
            GestorDeAudio.instancia.PausarSonido("Yellow");
            red.text = "";

            yield return new WaitForSeconds(0.5f);
            GestorDeAudio.instancia.ReproducirSonido("Narrador");
            narrador.text = "�Cu�les bloques?";
            yield return new WaitForSeconds(2);
            GestorDeAudio.instancia.PausarSonido("Narrador");
            narrador.text = "";

            yield return new WaitForSeconds(2f);
            GestorDeAudio.instancia.ReproducirSonido("Narrador");
            narrador.text = "Ya veo...";
            yield return new WaitForSeconds(2);
            GestorDeAudio.instancia.PausarSonido("Narrador");
            narrador.text = "";

            yield return new WaitForSeconds(0.5f);
            GestorDeAudio.instancia.ReproducirSonido("Yellow");
            red.text = "Ni�o, escala hasta la llave. Quiz�s puedas darle otro uso a la perspectiva para reducir su tama�o cuando est�s arriba.";
            yield return new WaitForSeconds(8);
            GestorDeAudio.instancia.PausarSonido("Yellow");
            red.text = "";

            yield return new WaitForSeconds(0.5f);
            GestorDeAudio.instancia.ReproducirSonido("Yellow");
            red.text = "Y no te preocupes, no te har�s da�o si te caes. No puedes morir en un sue�o.";
            yield return new WaitForSeconds(5);
            GestorDeAudio.instancia.PausarSonido("Yellow");
            red.text = "";

            yield return new WaitForSeconds(1f);
            GestorDeAudio.instancia.ReproducirSonido("Narrador");
            narrador.text = "Bueno, ya escuchaste al experto...";
            yield return new WaitForSeconds(4);
            GestorDeAudio.instancia.PausarSonido("Narrador");
            narrador.text = "";
        }

        if (DoorInteraction.doorCount == 2)
        {
            DoorInteraction.doorCount = 3;
            yield return new WaitForSeconds(1);
            GestorDeAudio.instancia.ReproducirSonido("Narrador");
            narrador.text = "Una puerta detr�s de otra puerta... �Es en serio?";
            yield return new WaitForSeconds(5);
            GestorDeAudio.instancia.PausarSonido("Narrador");
            narrador.text = "";

            yield return new WaitForSeconds(0.5f);
            GestorDeAudio.instancia.ReproducirSonido("Yellow");
            red.text = "�Parece que necesitan otra llave!";
            yield return new WaitForSeconds(3);
            GestorDeAudio.instancia.PausarSonido("Yellow");
            red.text = "";

            yield return new WaitForSeconds(0.5f);
            GestorDeAudio.instancia.ReproducirSonido("Narrador");
            narrador.text = "Cielos... mas les vale que el ayudante est� ah� dentro.";
            yield return new WaitForSeconds(4);
            GestorDeAudio.instancia.PausarSonido("Narrador");
            narrador.text = "";

            yield return new WaitForSeconds(0.5f);
            GestorDeAudio.instancia.ReproducirSonido("Yellow");
            yellow.text = "Solo dije que era una posibilidad...";
            yield return new WaitForSeconds(3);
            GestorDeAudio.instancia.PausarSonido("Yellow");
            yellow.text = "";

            yield return new WaitForSeconds(0.5f);
            GiantKey2.SetActive(true);
            GestorDeAudio.instancia.ReproducirSonido("Yellow");
            red.text = "Quiz�s esa llave que casualmente acaba de aparecer pueda serles de utilidad.";
            yield return new WaitForSeconds(5);
            GestorDeAudio.instancia.PausarSonido("Yellow");
            red.text = "";

            yield return new WaitForSeconds(0.5f);
            GestorDeAudio.instancia.ReproducirSonido("Narrador");
            narrador.text = "�Por qu� aparecen de la nada? �Y por qu� son gigantes?";
            yield return new WaitForSeconds(4);
            GestorDeAudio.instancia.PausarSonido("Narrador");
            narrador.text = "";

            yield return new WaitForSeconds(0.5f);
            GestorDeAudio.instancia.ReproducirSonido("Yellow");
            red.text = "�La magia de los sue�os!";
            yield return new WaitForSeconds(3);
            GestorDeAudio.instancia.PausarSonido("Yellow");
            red.text = "";

            yield return new WaitForSeconds(0.5f);
            GestorDeAudio.instancia.ReproducirSonido("Narrador");
            narrador.text = "Muy gracioso...";
            yield return new WaitForSeconds(1.5f);
            GestorDeAudio.instancia.PausarSonido("Narrador");
            narrador.text = "";

            yield return new WaitForSeconds(0.5f);
            GestorDeAudio.instancia.ReproducirSonido("Narrador");
            narrador.text = "Ni�o, puede que EL OJO nos vuelva a ser de utilidad. Pero, esa casa nos obstruye la vista...";
            yield return new WaitForSeconds(6f);
            GestorDeAudio.instancia.PausarSonido("Narrador");
            narrador.text = "";
            HouseCollider.SetActive(true);
        }

        if (control == true && counter == 11)
        {
            control = false;
            yield return new WaitForSeconds(1);
            GestorDeAudio.instancia.ReproducirSonido("Narrador");
            narrador.text = "Cada vez me impresionas mas...";
            yield return new WaitForSeconds(4);
            GestorDeAudio.instancia.PausarSonido("Narrador");
            narrador.text = "";
        }

        if (DoorInteraction.doorCount == 4)
        {
            DoorInteraction.doorCount = 5;
            yield return new WaitForSeconds(2);
            GestorDeAudio.instancia.ReproducirSonido("Narrador");
            narrador.text = "Bueno... esto no es una cocina...";
            yield return new WaitForSeconds(3);
            GestorDeAudio.instancia.PausarSonido("Narrador");
            narrador.text = "";

            yield return new WaitForSeconds(0.5f);
            GestorDeAudio.instancia.ReproducirSonido("Narrador");
            narrador.text = "Y tampoco hay se�ales de nuestro amigo...";
            yield return new WaitForSeconds(3);
            GestorDeAudio.instancia.PausarSonido("Narrador");
            narrador.text = "";

            yield return new WaitForSeconds(0.5f);
            GestorDeAudio.instancia.ReproducirSonido("Yellow");
            yellow.text = "�Dije que era una posibilidad!";
            yield return new WaitForSeconds(3);
            GestorDeAudio.instancia.PausarSonido("Yellow");
            yellow.text = "";

            yield return new WaitForSeconds(0.5f);
            GestorDeAudio.instancia.ReproducirSonido("Narrador");
            narrador.text = "�Y por qu� el reflejo est� al rev�s?";
            yield return new WaitForSeconds(3);
            GestorDeAudio.instancia.PausarSonido("Narrador");
            narrador.text = "";

            yield return new WaitForSeconds(0.5f);
            GestorDeAudio.instancia.ReproducirSonido("Yellow");
            red.text = "Magia... sue�os... blah, blah.";
            yield return new WaitForSeconds(3);
            GestorDeAudio.instancia.PausarSonido("Yellow");
            red.text = "";

            yield return new WaitForSeconds(0.5f);
            GestorDeAudio.instancia.ReproducirSonido("Narrador");
            narrador.text = "�Y ahora qu� hacemos?";
            yield return new WaitForSeconds(3);
            GestorDeAudio.instancia.PausarSonido("Narrador");
            narrador.text = "";
        }

        if (MirrorIsInverted == true)
        {
            MirrorIsInverted = false;
            yield return new WaitForSeconds(0.5f);
            GestorDeAudio.instancia.ReproducirSonido("Narrador");
            narrador.text = "Mucho mejor... �Dios m�o, ahora el ba�o est� al rev�s!";
            yield return new WaitForSeconds(4);
            GestorDeAudio.instancia.PausarSonido("Narrador");
            narrador.text = "";


            yield return new WaitForSeconds(5.5f);
            GestorDeAudio.instancia.ReproducirSonido("Yellow");
            Bk.text = "�Ya era hora!";
            yield return new WaitForSeconds(2);
            GestorDeAudio.instancia.PausarSonido("Yellow");
            Bk.text = "";

            yield return new WaitForSeconds(0.5f);
            GestorDeAudio.instancia.ReproducirSonido("Narrador");
            narrador.text = "�Ah� est�s!";
            yield return new WaitForSeconds(2);
            GestorDeAudio.instancia.PausarSonido("Narrador");
            narrador.text = "";

            yield return new WaitForSeconds(0.5f);
            GestorDeAudio.instancia.ReproducirSonido("Narrador");
            narrador.text = "�Qu� hac�as en el inodoro?";
            yield return new WaitForSeconds(2);
            GestorDeAudio.instancia.PausarSonido("Narrador");
            narrador.text = "";

            yield return new WaitForSeconds(0.5f);
            GestorDeAudio.instancia.ReproducirSonido("Yellow");
            Bk.text = "�La magia de los sue�os!";
            yield return new WaitForSeconds(2);
            GestorDeAudio.instancia.PausarSonido("Yellow");
            Bk.text = "";

            yield return new WaitForSeconds(0.5f);
            GestorDeAudio.instancia.ReproducirSonido("Narrador");
            narrador.text = "�Dejen de repetir eso todo el tiempo!";
            yield return new WaitForSeconds(2);
            GestorDeAudio.instancia.PausarSonido("Narrador");
            narrador.text = "";

            yield return new WaitForSeconds(1);
            GestorDeAudio.instancia.ReproducirSonido("Yellow");
            Bk.text = "�Por qu� mi reflejo est� en el techo?";
            yield return new WaitForSeconds(3);
            GestorDeAudio.instancia.PausarSonido("Yellow");
            Bk.text = "";

            yield return new WaitForSeconds(0.5f);
            GestorDeAudio.instancia.ReproducirSonido("Narrador");
            narrador.text = "No hay tiempo para eso. Necesitamos tu ayuda para que el ni�o pueda despertar. Ya ha pasado mucho tiempo.";
            yield return new WaitForSeconds(6);
            GestorDeAudio.instancia.PausarSonido("Narrador");
            narrador.text = "";

            yield return new WaitForSeconds(0.5f);
            GestorDeAudio.instancia.ReproducirSonido("Yellow");
            Bk.text = "Okay, es bastante simple de hecho.";
            yield return new WaitForSeconds(2);
            GestorDeAudio.instancia.PausarSonido("Yellow");
            Bk.text = "";

            yield return new WaitForSeconds(0.5f);
            GestorDeAudio.instancia.ReproducirSonido("Yellow");
            Bk.text = "Solo tienes que dormirte en la cama, y as� te despertar�s en el otro lado, en tu cuarto.";
            yield return new WaitForSeconds(5);
            GestorDeAudio.instancia.PausarSonido("Yellow");
            Bk.text = "";

            yield return new WaitForSeconds(0.5f);
            GestorDeAudio.instancia.ReproducirSonido("Yellow");
            Bk.text = "Y para hacer tu tarea mas f�cil, encontrar�s una casa gigante en medio de la Central.";
            yield return new WaitForSeconds(5);
            GestorDeAudio.instancia.PausarSonido("Yellow");
            Bk.text = "";

            yield return new WaitForSeconds(1f);
            GestorDeAudio.instancia.ReproducirSonido("Narrador");
            narrador.text = "�Perfecto! Ni�o, ya sabes que hacer.";
            yield return new WaitForSeconds(4);
            GestorDeAudio.instancia.PausarSonido("Narrador");
            narrador.text = "";
            HouseCollider2.SetActive(true);
            LittleHouse.SetActive(false);
        }

    }
}
