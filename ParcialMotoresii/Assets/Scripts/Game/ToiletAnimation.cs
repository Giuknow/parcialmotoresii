using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToiletAnimation : MonoBehaviour
{
    public static bool anim = false;
    public GameObject ToiletLib;
    public GameObject ToiletLibOpened;
    public GameObject BlackGhost;
    public GameObject ToiletLibR;
    public GameObject ToiletLibOpenedR;
    public GameObject BlackGhostR;
    public Animator animator;
    void Start()
    {
        ToiletLibOpened.SetActive(false);
        BlackGhost.SetActive(false);
        ToiletLibOpenedR.SetActive(false);
        BlackGhostR.SetActive(false);
    }
   
    void Update()
    {
        Animacion();
    }

    private void Animacion()
    {
        if (anim == true)
        {
            StartCoroutine(Wait());
        }
    }

    IEnumerator Wait()
    {
        anim = false;
        yield return new WaitForSeconds(6.5f);
        GestorDeAudio.instancia.ReproducirSonido("Toilet");
        yield return new WaitForSeconds(0.5f);
        ToiletLibOpened.SetActive(true);
        ToiletLib.SetActive(false);
        yield return new WaitForSeconds(2);
        BlackGhost.SetActive(true);
        GestorDeAudio.instancia.ReproducirSonido("Splash");
        animator.Play("BlackGhost");
        ToiletLibR.SetActive(true);
        ToiletLibOpenedR.SetActive(true);
        BlackGhostR.SetActive(true);
    }
}
