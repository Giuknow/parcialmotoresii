using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Perspectiva1 : MonoBehaviour
{
    public bool inCollision;
    public GameObject ghostInMachine;
    public GameObject ghost_Perspective;

    void Update()
    {
        if (inCollision && Machine.ItemsInMachine == 1 && Interacciones.pickCounter == 2)
        {
            GestorDeAudio.instancia.ReproducirSonido("Pop");
            ghostInMachine.SetActive(false);
            ghost_Perspective.SetActive(true);
            Machine.ItemsInMachine = 0;
            Narrador.control = true;
            Narrador.counter++;
        }
    }
    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            inCollision = true;
        }
    }
}
