using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Perspectiva5 : MonoBehaviour
{
    public bool inCollision;
    public static bool Act;
    public GameObject HouseInGround;
    public GameObject HouseInMachine;
    public GameObject HouseCollider;

    void Start()
    {
        HouseCollider.SetActive(false);
    }
    void Update()
    {
        if (inCollision && Interacciones.pickCounter == 9)
        {
            GestorDeAudio.instancia.ReproducirSonido("Pop");
            HouseInGround.SetActive(false);
            HouseInMachine.SetActive(true);
            HouseCollider.SetActive(true);
            Narrador.counter++;
            Narrador.control = true; ;
            this.gameObject.SetActive(false);
        }
    }
    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            inCollision = true;
        }
    }
    public void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            inCollision = false;
        }
    }


}
