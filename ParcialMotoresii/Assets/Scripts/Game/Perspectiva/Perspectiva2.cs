using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Perspectiva2 : MonoBehaviour
{
    public bool inCollision;
    public GameObject houseInMachine;
    public GameObject house_Perspective;

    void Start()
    {
    }
    void Update()
    {
        if (inCollision && Machine.ItemsInMachine == 1 && Interacciones.pickCounter == 3)
        {
            GestorDeAudio.instancia.ReproducirSonido("Pop");
            houseInMachine.SetActive(false);
            house_Perspective.SetActive(true);
            Narrador.counter++;
            Narrador.control = true;
            Machine.ItemsInMachine = 0;
        }

    }
    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            inCollision = true;
        }
    }
}
