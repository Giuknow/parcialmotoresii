using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RemoveHouse : MonoBehaviour
{
    public GameObject Key;
    public GameObject HouseGround;

    void Start()
    {
        Key.SetActive(false);
        HouseGround.SetActive(false);
    }
}
