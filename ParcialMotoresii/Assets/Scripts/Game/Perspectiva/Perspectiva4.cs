using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Perspectiva4 : MonoBehaviour
{
    public bool ctrl = true;
    public GameObject KeyGiant;
    public GameObject LEGO;
    public static bool ghostRED = false;

    public bool inCollision;
    public GameObject RghostInMachine;
    public GameObject Rghost_Perspective;

    void Start()
    {
        LEGO.SetActive(false);
        KeyGiant.SetActive(false);
    }
    void Update()
    {
        LEGOBricksInterlude();

        if (inCollision && Machine.ItemsInMachine == 1 && Interacciones.pickCounter == 8)
        {
            GestorDeAudio.instancia.ReproducirSonido("Pop");
            RghostInMachine.SetActive(false);
            Rghost_Perspective.SetActive(true);
            ghostRED = true;
            Narrador.counter++;
            Narrador.control = true;
            Machine.ItemsInMachine = 0;
        }
    }
    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            inCollision = true;
        }
    }
    private void LEGOBricksInterlude()
    {
        if (ghostRED == true)
        {
            ghostRED = false;
            StartCoroutine(Interlude());
        }
    }

    IEnumerator Interlude()
    {
        if (ctrl)
        {
            ctrl = false;
            yield return new WaitForSeconds(35f);
            KeyGiant.SetActive(true);
            yield return new WaitForSeconds(27f);
            LEGO.SetActive(true);
            GestorDeAudio.instancia.ReproducirSonido("Aparicion");
        }
    }
}

