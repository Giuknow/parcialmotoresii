using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Perspectiva3 : MonoBehaviour
{
    public bool inCollision;
    public GameObject Key1InMachine;
    public GameObject Key2InMachine;
    public GameObject Key3InMachine;
    public GameObject Key1Perspective;

    void Update()
    {
        if (inCollision && Machine.ItemsInMachine == 3 && Interacciones.pickCounter == 6)
        {
            GestorDeAudio.instancia.ReproducirSonido("Pop");
            Key1InMachine.SetActive(false);
            Key2InMachine.SetActive(false);
            Key3InMachine.SetActive(false);
            Key1Perspective.SetActive(true);
            Narrador.counter++;
            Narrador.control = true;
            Machine.ItemsInMachine = 0;
        }
    }
    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            inCollision = true;
        }
    }
}
