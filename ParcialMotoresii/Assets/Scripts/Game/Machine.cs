using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Machine : MonoBehaviour
{
    public static int ItemsInMachine = 0;
    public bool canBeUsed;
    //Ghost
    public GameObject ghostPicked;
    public GameObject ghostInMachine;
    public GameObject ghostPerspective;
    public GameObject RghostPicked;
    public GameObject RghostInMachine;
    public GameObject RghostPerspective;
    //House
    public GameObject housePicked;
    public GameObject houseInMachine;
    public GameObject housePerspective;
    //Key
    public GameObject Key1InMachine;
    public GameObject Key2InMachine;
    public GameObject Key3InMachine;
    public GameObject KeyPiecePicked;
    public GameObject Key1Perspective;


    void Start()
    {
        ghostInMachine.SetActive(false);
        ghostPerspective.SetActive(false);
        RghostPicked.SetActive(false);
        RghostInMachine.SetActive(false);
        RghostPerspective.SetActive(false);
        houseInMachine.SetActive(false);
        housePerspective.SetActive(false);
        Key1InMachine.SetActive(false);
        Key2InMachine.SetActive(false);
        Key3InMachine.SetActive(false);
        Key1Perspective.SetActive(false);
    }
    void Update()
    {
        ControlMachine();
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            canBeUsed = true;
        }
    }

    public void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            canBeUsed = false;
        }
    }

    private void ControlMachine()
    {
        if (canBeUsed && Input.GetKeyDown(KeyCode.E))
        {
            if (Interacciones.pickCounter == 2 && ItemsInMachine == 0)
            {
                ItemsInMachine += 1;
                ghostPicked.SetActive(false);
                ghostInMachine.SetActive(true);
                Narrador.control = true; Narrador.counter++;
            }
            if (Interacciones.pickCounter == 3 && ItemsInMachine == 0)
            {
                ItemsInMachine += 1;
                housePicked.SetActive(false);
                houseInMachine.SetActive(true);
                Narrador.counter++;
                Narrador.control = true;
            }
            if (Interacciones.pickCounter == 4 && ItemsInMachine == 0)
            {
                ItemsInMachine += 1;
                KeyPiecePicked.SetActive(false);
                Key1InMachine.SetActive(true);
            }
            if (Interacciones.pickCounter == 5 && ItemsInMachine == 1)
            {
                ItemsInMachine += 1;
                KeyPiecePicked.SetActive(false);
                Key2InMachine.SetActive(true);
            }
            if (Interacciones.pickCounter == 6 && ItemsInMachine == 2)
            {
                ItemsInMachine += 1;
                KeyPiecePicked.SetActive(false);
                Key3InMachine.SetActive(true);
                Narrador.counter++;
                Narrador.control = true;
            }
            if (Interacciones.pickCounter == 8 && ItemsInMachine == 0)
            {
                ItemsInMachine += 1;
                RghostPicked.SetActive(false);
                RghostInMachine.SetActive(true);
            }





        }
    }
}
