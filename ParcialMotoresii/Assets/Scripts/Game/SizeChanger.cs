using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SizeChanger : MonoBehaviour
{
    public GameObject house;
    public GameObject bedroom;
    public GameObject collider1;
    public GameObject Wall;

    void Start()
    {
        house.SetActive(false); 
        Wall.SetActive(false);
    }

    void Update()
    {
        
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            AnimatorManager.event1 = true;
            house.SetActive(true);
            bedroom.SetActive(false);
            collider1.SetActive(false);
            Wall.SetActive(true);
            GestorDeAudio.instancia.ReproducirSonido("Transition1");
            Narrador.counter++;
            Narrador.control = true;
            this.gameObject.SetActive(false);
        }
    }
}
