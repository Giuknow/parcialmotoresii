using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuScreen : MonoBehaviour
{
    public bool state = true;
    public static int play = 0;
    public GameObject Menu;
    public GameObject Menuscreen;
    public GameObject SongButtoms;
    public GameObject Esp;
    public GameObject Eng;
    public GameObject MisteryObject;
    void Start()
    {
        GestorDeAudio.instancia.ReproducirSonido("Epilogue");
        SongButtoms.SetActive(false);
        Esp.SetActive(false);
        Eng.SetActive(false);
        MisteryObject.SetActive(false);
    }
    void Update()
    {
    }
    public void Jugar()
    {
        play = 1;
        GestorDeAudio.instancia.PausarSonido("Epilogue");
        Menu.SetActive(false);
        Menuscreen.SetActive(false);
    }

    public void Letra()
    {
        SongButtoms.SetActive(true);
        Eng.SetActive(true);
        Menu.SetActive(false);
        Menuscreen.SetActive(false);
    }

    public void CambiarIdioma()
    {
        state = !state;
        if (state)
        {
            Eng.SetActive(false);
            Esp.SetActive(true);
        }
        else
        {
            Eng.SetActive(true);
            Esp.SetActive(false);
        }
    }

    public void Back()
    {
        SongButtoms.SetActive(false);
        Eng.SetActive(false);
        Esp.SetActive(false);
        Menu.SetActive(true);
        Menuscreen.SetActive(true);
    }
}
